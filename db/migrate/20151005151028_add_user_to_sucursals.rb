class AddUserToSucursals < ActiveRecord::Migration[6.0]
  def change
    add_reference :sucursals, :user, index: true
  end
end
