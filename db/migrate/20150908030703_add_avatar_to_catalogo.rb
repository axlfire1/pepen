class AddAvatarToCatalogo < ActiveRecord::Migration[6.0]
  def change
    add_column :catalogos, :avatar, :string
  end
end
