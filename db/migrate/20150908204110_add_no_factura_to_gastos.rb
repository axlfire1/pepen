class AddNoFacturaToGastos < ActiveRecord::Migration[6.0]
  def change
    add_column :gastos, :no_factura, :integer
  end
end
