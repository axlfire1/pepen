class AddAlmacenToSucursals < ActiveRecord::Migration[6.0]
  def change
    add_column :sucursals, :almacen, :boolean, null: false, default: false
  end
end
