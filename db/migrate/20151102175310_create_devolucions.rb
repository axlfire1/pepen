class CreateDevolucions < ActiveRecord::Migration[6.0]
  def change
    create_table :devolucions do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :catalogo, index: true, foreign_key: true
      t.integer :cantidad
      t.belongs_to :sucursal, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
