class AddNoFacturaToCostos < ActiveRecord::Migration[6.0]
  def change
    add_column :costos, :no_factura, :integer
  end
end
