class AddFechaToGastos < ActiveRecord::Migration[6.0]
  def change
    add_column :gastos, :fecha, :date
  end
end
