class CreateSucursals < ActiveRecord::Migration[6.0]
  def change
    create_table :sucursals do |t|
      t.string :nombre_sucursal
      t.string :responsable
      t.integer :telefono
      t.text :direccion
      t.string :pais

      t.timestamps
    end
  end
end
