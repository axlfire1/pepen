class SetNotNullsToCatalogos < ActiveRecord::Migration[6.0]


  def up
      change_column_default :catalogos, :precio_vendedor, 0
      change_column_default :catalogos, :precio_publico, 0
      change_column_default :catalogos, :costo_produccion, 0
      change_column_default :costos, :monto, 0
      change_column_default :gastos, :monto, 0
      change_column_default :venta, :cantidad, 0
      change_column_default :venta, :monto, 0
      
  end
  
  def down
    
  end
  
end
