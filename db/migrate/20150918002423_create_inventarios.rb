class CreateInventarios < ActiveRecord::Migration[6.0]
  def change
    create_table :inventarios do |t|
      t.references :catalogo, index: true
      t.integer :cantidad
      t.references :sucursal, index: true

      t.timestamps
    end
  end
end
