class CreateCatalogos < ActiveRecord::Migration[6.0]
  def change
    create_table :catalogos do |t|
      t.references :categoria, index: true
      t.string :nombre_producto
      t.string :marca
      t.text :descripcion
      t.string :medidas
      t.float :precio_vendedor
      t.float :precio_publico
      t.float :costo_produccion

      t.timestamps
    end
  end
end
