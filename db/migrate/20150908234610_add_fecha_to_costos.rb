class AddFechaToCostos < ActiveRecord::Migration[6.0]
  def change
    add_column :costos, :fecha, :date
  end
end
