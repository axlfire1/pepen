class CreateGastos < ActiveRecord::Migration[6.0]
  def change
    create_table :gastos do |t|
      t.string :concepto
      t.text :descripcion
      t.float :monto

      t.timestamps
    end
  end
end
