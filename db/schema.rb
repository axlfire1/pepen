# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2016_02_16_033138) do

  create_table "catalogos", force: :cascade do |t|
    t.integer "categoria_id"
    t.string "nombre_producto"
    t.string "marca"
    t.text "descripcion"
    t.string "medidas"
    t.float "precio_vendedor", default: 0.0
    t.float "precio_publico", default: 0.0
    t.float "costo_produccion", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "avatar"
    t.index ["categoria_id"], name: "index_catalogos_on_categoria_id"
  end

  create_table "categoria", force: :cascade do |t|
    t.string "nombre"
    t.text "descripcion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "costos", force: :cascade do |t|
    t.string "concepto"
    t.text "descripcion"
    t.float "monto", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "no_factura"
    t.date "fecha"
  end

  create_table "devolucions", force: :cascade do |t|
    t.integer "user_id"
    t.integer "catalogo_id"
    t.integer "cantidad"
    t.integer "sucursal_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["catalogo_id"], name: "index_devolucions_on_catalogo_id"
    t.index ["sucursal_id"], name: "index_devolucions_on_sucursal_id"
    t.index ["user_id"], name: "index_devolucions_on_user_id"
  end

  create_table "gastos", force: :cascade do |t|
    t.string "concepto"
    t.text "descripcion"
    t.float "monto", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "no_factura"
    t.date "fecha"
  end

  create_table "inventarios", force: :cascade do |t|
    t.integer "catalogo_id"
    t.integer "cantidad"
    t.integer "sucursal_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["catalogo_id"], name: "index_inventarios_on_catalogo_id"
    t.index ["sucursal_id"], name: "index_inventarios_on_sucursal_id"
  end

  create_table "sucursals", force: :cascade do |t|
    t.string "nombre_sucursal"
    t.integer "telefono"
    t.text "direccion"
    t.string "pais"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.string "ciudad"
    t.boolean "almacen", default: false, null: false
    t.index ["user_id"], name: "index_sucursals_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "role"
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "venta", force: :cascade do |t|
    t.integer "user_id"
    t.integer "catalogo_id"
    t.integer "cantidad", default: 0
    t.integer "sucursal_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "monto", default: 0.0
    t.index ["catalogo_id"], name: "index_venta_on_catalogo_id"
    t.index ["sucursal_id"], name: "index_venta_on_sucursal_id"
    t.index ["user_id"], name: "index_venta_on_user_id"
  end

  add_foreign_key "devolucions", "catalogos"
  add_foreign_key "devolucions", "sucursals"
  add_foreign_key "devolucions", "users"
  add_foreign_key "venta", "catalogos"
  add_foreign_key "venta", "sucursals"
  add_foreign_key "venta", "users"
end
