json.array!(@catalogos) do |catalogo|
  json.extract! catalogo, :id, :categoria_id, :nombre_producto, :marca, :descripcion, :medidas, :precio_vendedor, :precio_publico, :costo_produccion
  json.url catalogo_url(catalogo, format: :json)
end
