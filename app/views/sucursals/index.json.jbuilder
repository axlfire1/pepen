json.array!(@sucursals) do |sucursal|
  json.extract! sucursal, :id, :nombre_sucursal, :responsable, :telefono, :direccion, :pais
  json.url sucursal_url(sucursal, format: :json)
end
