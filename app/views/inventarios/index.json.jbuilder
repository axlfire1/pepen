json.array!(@inventarios) do |inventario|
  json.extract! inventario, :id, :catalogo_id, :cantidad, :sucursal_id
  json.url inventario_url(inventario, format: :json)
end
