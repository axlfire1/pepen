json.array!(@gastos) do |gasto|
  json.extract! gasto, :id, :concepto, :descripcion, :monto
  json.url gasto_url(gasto, format: :json)
end
