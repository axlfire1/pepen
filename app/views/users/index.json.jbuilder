json.array!(@users) do |user|
  json.extract! user, :id, :email, :password, :confirmation_password, :role
  json.url user_url(user, format: :json)
end
