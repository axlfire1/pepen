json.array!(@costos) do |costo|
  json.extract! costo, :id, :concepto, :descripcion, :monto
  json.url costo_url(costo, format: :json)
end
