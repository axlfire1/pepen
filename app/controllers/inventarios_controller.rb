class InventariosController < ApplicationController
  before_action :set_inventario, only: [:show, :edit, :update, :destroy]
  before_action :validate_does_admin
  respond_to :html

  def index
    @inventarios = Inventario.joins(:sucursal).order("max(sucursals.nombre_sucursal) ASC").group("inventarios.id")    
    respond_with(@inventarios)
  end

  def show
    respond_with(@inventario)
  end

  def new
    @inventario = Inventario.new
    respond_with(@inventario)
  end

  def edit
  end

  def create
    @temp = Inventario.new(inventario_params)
    unless @temp.sucursal.almacen
      almacen = Sucursal.almacen.first
      unless almacen
        flash[:notice] = '¡No hay un almacen definido!'
        @inventario = @temp
        render :new and return
      end
      inventario_almacen = almacen.inventarios.where(catalogo_id: @temp.catalogo_id).first
      unless inventario_almacen
        flash[:notice] = '¡El almacen no tiene productos!'
        @inventario = @temp
        render :new and return
      end
      if inventario_almacen.cantidad < @temp.cantidad
        flash[:notice] = '¡El almacen no tiene suficientes productos!'
        @inventario = @temp
        render :new and return
      end
      inventario_almacen.cantidad -= @temp.cantidad
      inventario_almacen.save
    end
    @inventario = @temp.sucursal.inventarios.where(catalogo_id: @temp.catalogo_id).first_or_initialize 
    @inventario.cantidad ||= 0
    @inventario.cantidad += @temp.cantidad
    
    @inventario.save
    respond_with(@inventario)
  end

  def update
    @inventario.update(inventario_params)
    respond_with(@inventario)
  end

  def destroy
    @inventario.destroy
    respond_with(@inventario)
  end

  private
  def set_inventario
    @inventario = Inventario.find(params[:id])
  end

  def inventario_params
    params.require(:inventario).permit(:catalogo_id, :cantidad, :sucursal_id)
  end
end
