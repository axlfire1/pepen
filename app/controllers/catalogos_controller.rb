class CatalogosController < ApplicationController
  before_action :set_catalogo, only: [:show, :edit, :update, :destroy]
  before_action :validate_does_admin
  def index   
    @catalogos = Catalogo.all.order(nombre_producto: :asc)
    respond_to do |format|
      format.html 
      format.pdf { render_catalogo_list(@catalogos)}
    end
  end
        
  def show
    #respond_with(@catalogo)
  end

  def new
    @catalogo = Catalogo.new
  end

  def edit
  
  end

  def create
    @catalogo = Catalogo.new(catalogo_params)
      respond_to do |format|
      if @catalogo.save  
        format.html {redirect_to @catalogo} 
      else 
        format.html {render :new}
      end
    end
  end

  def update
    respond_to do |format|
      if @catalogo.update(catalogo_params)  
        format.html {redirect_to @catalogo} 
      else 
        format.html {render :edit}
      end
    end
  end

  def destroy
    @catalogo.destroy
    redirect_to (catalogos_path)
  end

  private
  def set_catalogo
    @catalogo = Catalogo.find(params[:id])
  end

  def catalogo_params
    params.require(:catalogo).permit(:categoria_id, :nombre_producto, :marca, :descripcion, :medidas, :precio_vendedor, :precio_publico, :costo_produccion, :avatar)
  end
  
  
  def render_catalogo_list(catalogo)
    report = ThinReports::Report.new layout: File.join(Rails.root, 'app', 'reports', 'report.tlf')

    catalogo.each do |c|
      report.list.add_row do |row|
        row.values clave: c.id, 
          categoria: c.categoria, 
          producto: c.nombre_producto, 
          descripcion: c.descripcion, 
          marca: c.marca,
          medidas: c.medidas,
          precio_vendedor: c.precio_vendedor,
          precio_publico: c.precio_publico
#          row.item(:imagen).value('public/uploads/catalogo/avatar/16/Screenshot_from_2015-09-29_18-19-10.png')
         row.item(:imagen).value(c.avatar.path) unless c.avatar.to_s.blank?
      end
    end

      
    send_data report.generate, filename: 'catalogo_pepen.pdf', 
      type: 'application/pdf', 
      disposition: 'attachment'
  end

end

