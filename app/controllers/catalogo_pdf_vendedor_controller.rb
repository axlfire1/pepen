class CatalogoPdfVendedorController < ApplicationController
  def index
    @catalogos_usuario = Catalogo.all.order(nombre_producto: :asc)
    respond_to do |format|
      format.html 
      format.pdf { render_catalogo_list_vendedor(@catalogos_usuario)}
    end
  end


  def render_catalogo_list_vendedor(catalogo)
    report = ThinReports::Report.new layout: File.join(Rails.root, 'app', 'reports', 'report2.tlf')

    catalogo.each do |c|
      report.list.add_row do |row|
        row.values producto: c.nombre_producto,
        clave: c.id, 
        descripcion: c.descripcion,
        medidas: c.medidas,
        precio_publico: c.precio_publico
#        row.item(:imagen).value('public/uploads/catalogo/avatar/16/Screenshot_from_2015-09-29_18-19-10.png')
        row.item(:imagen).value(c.avatar.path) unless c.avatar.to_s.blank?
      end
    end

      
    send_data report.generate, filename: 'catalogo_pepen.pdf', 
      type: 'application/pdf', 
      disposition: 'attachment'
  end
end