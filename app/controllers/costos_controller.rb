class CostosController < ApplicationController
  before_action :set_costo, only: [:show, :edit, :update, :destroy]
  before_action :validate_does_admin
  
  respond_to :html

  def index
    @costos = Costo.all
    respond_with(@costos)
  end

  def show
    respond_with(@costo)
  end

  def new
    @costo = Costo.new
    respond_with(@costo)
  end

  def edit
  end

  def create
    @costo = Costo.new(costo_params)
    @costo.save
    respond_with(@costo)
  end

  def update
    @costo.update(costo_params)
    respond_with(@costo)
  end

  def destroy
    @costo.destroy
    respond_with(@costo)
  end

  private
    def set_costo
      @costo = Costo.find(params[:id])
    end

    def costo_params
      params.require(:costo).permit(:concepto, :descripcion, :monto, :no_factura, :fecha)
    end
end
