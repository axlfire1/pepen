class ResumeController < ApplicationController
  
  def index   
    @gastos_mensuales = (0..12).map do |i|
      Gasto.where('extract(month from fecha) = ?',i.to_s.rjust(2,'0')).where('extract(year from fecha)= ? ', Date.today.year).sum(:monto)
    end
    total_gastos = @gastos_mensuales.inject(:+)
        
    @costos_mensuales = (0..12).map do |i|
      Costo.where('extract(month from fecha) = ?',i.to_s.rjust(2,'0')).where('extract(year from fecha)= ? ', Date.today.year).sum(:monto)
    end 
    total_costos = @costos_mensuales.inject(:+)
    
    @ventas_ganancia_mensuales = (0..12).map do |i|
      Ventum.where('extract(month from created_at) = ? ',i.to_s.rjust(2,'0')).where('extract(year from created_at)= ?', Date.today.year).sum(:monto)
    end
    total_ventas_ganancias = @ventas_ganancia_mensuales.inject(:+)
        
    @costos_produccion_mensuales = (0..12).map do |i|
      is = Inventario.select(:cantidad, :catalogo_id, :created_at).where('extract(month from created_at) = ?  ',i.to_s.rjust(2,'0')).where('extract(year from created_at)= ? ', Date.today.year)
      cp = 0
      is.each do |inventario|
        cp+= inventario.cantidad * inventario.catalogo.costo_produccion
      end
      cp
    end 
        
    total_costos_produccion = @costos_produccion_mensuales.inject(:+)
    
    total_ingresos = total_ventas_ganancias + total_costos_produccion - total_gastos - total_costos
    
    #CUENTAS UNIDADES
    #ventas en unidades
    @ventas_mensuales = (0..12).map do |i|
      Ventum.where('extract(month from created_at) = ?  ',i.to_s.rjust(2,'0')).where('extract(year from created_at)= ? ', Date.today.year).sum(:cantidad)
    end 
    total_venta_anual = @ventas_mensuales.inject(:+)
    
    @devoluciones_mensuales = (0..12).map do |i|
      Devolucion.where('extract(month from created_at) = ? ',i.to_s.rjust(2,'0')).where('extract(year from created_at)= ? ', Date.today.year).sum(:cantidad)
    end 
    total_devolucion = @devoluciones_mensuales.inject(:+)
        
    respond_to do |format|
      format.html 
      format.pdf {render_resume_pdf(@gastos_mensuales, total_gastos, @costos_mensuales, total_costos, @costos_produccion_mensuales, total_costos_produccion, @devoluciones_mensuales, total_devolucion, @ventas_mensuales, total_venta_anual, @ventas_ganancia_mensuales, total_ventas_ganancias, total_ingresos)}      
      format.csv { render_resume_csv(@ventas_mensuales, @devoluciones_mensuales, @gastos_mensuales, @costos_mensuales, @ventas_ganancia_mensuales, @costos_produccion_mensuales,total_gastos, total_costos, total_ventas_ganancias, total_costos_produccion, total_venta_anual, total_devolucion, total_ingresos).to_a }
    end
  end
   
  
  def render_resume_csv(ventas_mensuales, devoluciones_mensuales, gastos_mensuales, costos_mensuales, ventas_ganancia_mensuales, costos_produccion_mensuales, total_gastos, total_costos,total_ventas_ganancias, total_costos_produccion, total_venta_anual, total_devolucion, total_ingresos )
    CSV.open(File.join(Rails.root, 'app', 'reports', 'resume.csv'), "wb") do |csv|
      csv << ["CONCEPTO","ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT" , "NOV", "DIC", "TOTAL"]
      csv << ["Gastos", gastos_mensuales[0], gastos_mensuales[1], gastos_mensuales[2], gastos_mensuales[3], gastos_mensuales[4], gastos_mensuales[5], gastos_mensuales[6], gastos_mensuales[7], gastos_mensuales[8], gastos_mensuales[9], gastos_mensuales[10], gastos_mensuales[11], total_gastos]
      csv << ["Costos", costos_mensuales[0], costos_mensuales[1], costos_mensuales[2], costos_mensuales[3], costos_mensuales[4], costos_mensuales[5], costos_mensuales[6], costos_mensuales[7], costos_mensuales[8], costos_mensuales[9], costos_mensuales[10], costos_mensuales[11], total_costos ]
      csv << ["Ingreso por ventas", ventas_ganancia_mensuales[0], ventas_ganancia_mensuales[1], ventas_ganancia_mensuales[2], ventas_ganancia_mensuales[3], ventas_ganancia_mensuales[4], ventas_ganancia_mensuales[5], ventas_ganancia_mensuales[6], ventas_ganancia_mensuales[7], ventas_ganancia_mensuales[8], ventas_ganancia_mensuales[9], ventas_ganancia_mensuales[10], ventas_ganancia_mensuales[11], total_ventas_ganancias ]
      csv << ["Costo de Inventario", costos_produccion_mensuales[0], costos_produccion_mensuales[1], costos_produccion_mensuales[2], costos_produccion_mensuales[3], costos_produccion_mensuales[4], costos_produccion_mensuales[5], costos_produccion_mensuales[6], costos_produccion_mensuales[7], costos_produccion_mensuales[8], costos_produccion_mensuales[9], costos_produccion_mensuales[10], costos_produccion_mensuales[11], total_costos_produccion]
      csv << ["Total Ingresos", total_ingresos]
      csv << []
      csv << ["Ventas por Unidad", ventas_mensuales[0], ventas_mensuales[1], ventas_mensuales[2], ventas_mensuales[3], ventas_mensuales[4], ventas_mensuales[5], ventas_mensuales[6], ventas_mensuales[7], ventas_mensuales[8], ventas_mensuales[9], ventas_mensuales[10], ventas_mensuales[11], total_venta_anual]
      csv << ["Devluciones por Unidad", devoluciones_mensuales[0], devoluciones_mensuales[1], devoluciones_mensuales[2], devoluciones_mensuales[3], devoluciones_mensuales[4], devoluciones_mensuales[5], devoluciones_mensuales[6], devoluciones_mensuales[7], devoluciones_mensuales[8], devoluciones_mensuales[9], devoluciones_mensuales[10], devoluciones_mensuales[11],total_devolucion]
    end
    render file:   File.join(Rails.root, 'app', 'reports', 'resume.csv')
  end


  


  def render_resume_pdf(gastos, total_gastos, costos, total_costos, costos_produccion, costos_produccion_total, devoluciones_mes, total_devoluciones,ventas, total_ventas, v_dinero_menusal, v_dinero_total, total_ingresos)
    report = ThinReports::Report.new layout: File.join(Rails.root, 'app', 'reports', 'resume.tlf')
    
    # map gastos por mes
    gastos_por_mes = Hash[ *(1..12).collect { |i| ["total_gastos_#{i}".to_sym, gastos[i] ] }.flatten ]
    #    logger.debug gastos_por_mes
  
    # map costos por mes
    costos_por_mes = Hash[ *(1..12).collect { |i| ["total_costos_#{i}".to_sym, costos[i] ] }.flatten ]
    
    # map ventas dinero por mes
    v_dinero_menusal = Hash[ *(1..12).collect { |i| ["vd_#{i}".to_sym, v_dinero_menusal[i] ] }.flatten ]
    
    #map costo de produccion por mes
    costos_produccion_mes = Hash[ *(1..12).collect { |i| ["cp_#{i}".to_sym, costos_produccion[i] ] }.flatten ]
    
    # map devoluciones por mes
    devoluciones_mes = Hash[ *(1..12).collect { |i| ["dp_#{i}".to_sym, devoluciones_mes[i] ] }.flatten ]
    
    # map ventas por mes
    ventas_por_mes = Hash[ *(1..12).collect { |i| ["total_ventas_#{i}".to_sym, ventas[i] ] }.flatten ]
        
    report.list.add_row do |row|
      row.values gastos_por_mes
      row.values total_gastos: total_gastos
      
      row.values costos_por_mes
      row.values total_costos: total_costos
    
      row.values costos_produccion_mes
      row.values total_cp: costos_produccion_total
          
      row.values v_dinero_menusal
      row.values total_v: v_dinero_total
            
      row.values devoluciones_mes
      row.values dp_total: total_devoluciones
      
      row.values ventas_por_mes
      row.values total_ventas: total_ventas
      #row.values total_costos: costo
      
      row.values total_ingresos: total_ingresos
      
    end
    send_data report.generate, filename: 'reporte_pepen.pdf', 
      type: 'application/pdf', 
      disposition: 'attachment'
  end
end
