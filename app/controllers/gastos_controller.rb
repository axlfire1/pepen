class GastosController < ApplicationController
  before_action :set_gasto, only: [:show, :edit, :update, :destroy]
  before_action :validate_does_admin
  respond_to :html

  def index
    @gastos = Gasto.order(fecha: :desc)
    respond_with(@gastos)
  end

  def show
    respond_with(@gasto)
  end

  def new
    @gasto = Gasto.new
    respond_with(@gasto)
  end

  def edit
  end

  def create
    @gasto = Gasto.new(gasto_params)
    @gasto.save
    respond_with(@gasto)
  end

  def update
    @gasto.update(gasto_params)
    respond_with(@gasto)
  end

  def destroy
    @gasto.destroy
    respond_with(@gasto)
  end

  private
  def set_gasto
    @gasto = Gasto.find(params[:id])
  end

  def gasto_params
    params.require(:gasto).permit(:concepto, :descripcion, :monto, :no_factura, :fecha)
  end
end
