class ApplicationController < ActionController::Base
before_action :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  protect_from_forgery with: :exception

  protected
  def validate_does_admin
#    redirect_to authenticated_root_path, notice: 'No tienes permisos .|.' unless current_user.admin?
  end
  
end
