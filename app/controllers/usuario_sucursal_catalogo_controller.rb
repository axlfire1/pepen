class UsuarioSucursalCatalogoController < ApplicationController

  #   before_action :validate_does_admin

  def index
    @catalogos_usuario = current_user.sucursal.inventarios.all if current_user.sucursal
    respond_to do |format|
      format.html 
    end
  end
  
  
  def show
    @inventario = Inventario.find params[:id]
  end

  def movimiento
    catalogo = Catalogo.find params[:catalogo_id]
    sucursal = current_user.sucursal
    case params[:commit]
    when 'Venta'
      venta(current_user, sucursal, catalogo, params[:cantidad].to_i)
    when 'Devolucion'
      devolucion(current_user, sucursal, catalogo, params[:cantidad].to_i)
    end
    redirect_to usuario_sucursal_catalogo_index_path
  end
  
  private
  def catalogo_params
    params.require(:catalogo).permit(:categoria_id, :nombre_producto, :marca, :descripcion, :medidas, :precio_vendedor, :precio_publico, :costo_produccion, :avatar)
  end
  
  def venta(usuario, sucursal, catalogo, cantidad)
    inventario = sucursal.inventarios.where(catalogo_id: catalogo.id).first
    unless inventario
      flash[:notice] = 'No hay existencias del producto'
      return
    end
    if inventario.cantidad < cantidad
      flash[:notice] = 'No hay suficientes existencias del producto'
      return
    end
    inventario.cantidad -= cantidad
    inventario.save
    v = Ventum.new user_id: usuario.id, sucursal_id: sucursal.id, catalogo_id: catalogo.id, cantidad: cantidad, monto: catalogo.precio_vendedor*cantidad
    if v.save
      flash[:notice] = 'Venta exitosa'
    else
      flash[:notice] = 'No se pudo realizar la venta'
    end
  end
  
  def devolucion(usuario, sucursal, catalogo, cantidad)
    inventario = sucursal.inventarios.where(catalogo_id: catalogo.id).first
    unless inventario
      flash[:notice] = 'No hay existencias del producto'
      return
    end
    if inventario.cantidad < cantidad
      flash[:notice] = 'No hay suficientes existencias del producto'
      return
    end
    inventario.cantidad -= cantidad
    inventario.save
    almacen = Sucursal.almacen.first
    inventario_almacen = almacen.inventarios.where(catalogo_id: catalogo.id).first_or_initialize
    inventario_almacen.cantidad ||= 0
    inventario_almacen.cantidad += cantidad
    inventario_almacen.save
    v = Devolucion.new user_id: usuario.id, sucursal_id: sucursal.id, catalogo_id: catalogo.id, cantidad: cantidad
    if v.save
      flash[:notice] = 'Devolucion exitosa'
    else
      flash[:notice] = 'No se pudo realizar la devolucion'
    end
    
  end
  
  
  
  
  
end
