class Catalogo < ActiveRecord::Base
  has_many :inventarios, dependent: :destroy  
  belongs_to :categoria, class_name: 'Categorium'

  mount_uploader :avatar, AvatarUploader
  
  def to_s
    self.nombre_producto
  end
end
