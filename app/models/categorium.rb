class Categorium < ActiveRecord::Base
  has_many :catalogos,  dependent: :destroy,  foreign_key: "categoria_id"
  
  def to_s
    self.nombre
  end
end
