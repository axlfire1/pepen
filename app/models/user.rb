class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,# :registerable, (quito que searegistrable)
         :recoverable, :rememberable, :trackable, :validatable
  
   validates :role, presence: true
  
  has_one :sucursal, dependent: :destroy
  
  enum role: [:admin, :user]

  def to_s
    self.email
  end
end
