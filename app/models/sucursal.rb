class Sucursal < ActiveRecord::Base
  
  has_many :inventarios, dependent: :destroy
  belongs_to :user
  validates :user_id, uniqueness: true
  scope :almacen, -> { where(almacen: true) }
end
